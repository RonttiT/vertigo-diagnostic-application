package com.example.vertigodiagnosticapplication.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.vertigodiagnosticapplication.R;
import com.example.vertigodiagnosticapplication.global_variables.GVStore;
import com.example.vertigodiagnosticapplication.models.AccDataResponse;
import com.example.vertigodiagnosticapplication.models.MeasurementData;
import com.example.vertigodiagnosticapplication.models.MyScanResult;
import com.google.gson.Gson;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsNotificationListener;
import com.movesense.mds.MdsSubscription;
import com.polidea.rxandroidble2.RxBleClient;

import java.util.ArrayList;


public class ManualMeasurementSessionActivity extends AppCompatActivity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();



    // Sensor subscription
    static private String URI_MEAS_ACC_13 = "/Meas/IMU9/13";
    private MdsSubscription mdsSubscription;
    private String subscribedDeviceSerial;


    // Measurement start
    private boolean meas_active = false;

    // Timer
    Chronometer simpleChronometer;

    // MDS
    public static final String URI_CONNECTEDDEVICES = "suunto://MDS/ConnectedDevices";
    public static final String URI_EVENTLISTENER = "suunto://MDS/EventListener";
    public static final String SCHEME_PREFIX = "suunto://";
    //public static final String IMU_INFO_PATH = "/Meas/IMU/Info";

    // BleClient singleton
    static private RxBleClient mBleClient;




    // Main function that gets launched on the creation of this activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_measurement_session);
        simpleChronometer = (Chronometer) findViewById(R.id.simpleChronometer);

        // Chooses which sensor to use and subscribe to it. In the future this needs to be don based on the settings setup in the setup MeasurementSessionSetupActivity.
        MyScanResult device = GVStore.ScanResArrayList.get(0);
        subscribeToSensor(device.connectedSerial); // The structure of the test should be decided. When is it started, where is the data stored, step count? etc
        // Starting the Measurement
        startMeasurement();
        simpleChronometer.start(); // start a chronometer
    }


    // Function that subscribes to a service of the sensor
    private void subscribeToSensor(String connectedSerial) {
        // Clean up existing subscription (if there is one)
        if (mdsSubscription != null) {
            unsubscribe();
        }
        // Build JSON doc that describes what resource and device to subscribe
        // Here we subscribe to 13 hertz accelerometer data
        StringBuilder sb = new StringBuilder();
        String strContract = sb.append("{\"Uri\": \"").append(connectedSerial).append(URI_MEAS_ACC_13).append("\"}").toString();
        Log.d(LOG_TAG, strContract);

        // This should probably not be here
        final View sensorUI = findViewById(R.id.measurement_session_SensorUI);

        subscribedDeviceSerial = connectedSerial;

        mdsSubscription = GVStore.mds_obj.builder().build(this).subscribe(URI_EVENTLISTENER,
                strContract, new MdsNotificationListener() {
                    @Override
                    public void onNotification(String data) {
                        Log.d(LOG_TAG, "onNotification(): " + data);

                        // If UI not enabled, do it now
                        if (sensorUI.getVisibility() == View.GONE)
                            sensorUI.setVisibility(View.VISIBLE);

                        AccDataResponse accResponse = new Gson().fromJson(data, AccDataResponse.class);
                        if (accResponse != null && accResponse.body.acc_array.length > 0) {

                            String accStr = String.format("Acc: %.02f, %.02f, %.02f \n Gyr: %.02f, %.02f, %.02f \n Mag: %.02f, %.02f, %.02f ",
                                    accResponse.body.acc_array[0].x, accResponse.body.acc_array[0].y, accResponse.body.acc_array[0].z,
                                    accResponse.body.gyr_array[0].x, accResponse.body.gyr_array[0].y, accResponse.body.gyr_array[0].z,
                                    accResponse.body.mag_array[0].x, accResponse.body.mag_array[0].y, accResponse.body.mag_array[0].z);
                            ((TextView)findViewById(R.id.measurement_session_text_SensorMsg)).setText(accStr);

                            // Here we need to decide how to save the data.
                            GVStore.sensorOne_MeasData.acc_x.add(accResponse.body.acc_array[0].x);
                            GVStore.sensorOne_MeasData.acc_y.add(accResponse.body.acc_array[0].y);
                            GVStore.sensorOne_MeasData.acc_z.add(accResponse.body.acc_array[0].z);

                            GVStore.sensorOne_MeasData.gyr_x.add(accResponse.body.gyr_array[0].x);
                            GVStore.sensorOne_MeasData.gyr_y.add(accResponse.body.gyr_array[0].y);
                            GVStore.sensorOne_MeasData.gyr_z.add(accResponse.body.gyr_array[0].z);

                            GVStore.sensorOne_MeasData.mag_x.add(accResponse.body.mag_array[0].x);
                            GVStore.sensorOne_MeasData.mag_y.add(accResponse.body.mag_array[0].y);
                            GVStore.sensorOne_MeasData.mag_z.add(accResponse.body.mag_array[0].z);


                        }
                    }
                    @Override
                    public void onError(MdsException error) {
                        Log.e(LOG_TAG, "subscription onError(): ", error);
                        unsubscribe();
                    }
                });
    }

    // Function that unsubscribes - for the moment ony used in case of Errors when getting the data
    public void unsubscribe() {
        if (mdsSubscription != null) {
            mdsSubscription.unsubscribe();
            mdsSubscription = null;
        }
        subscribedDeviceSerial = null;
        // If UI not invisible, do it now
        final View sensorUI = findViewById(R.id.measurement_session_SensorUI);
        if (sensorUI.getVisibility() != View.GONE)
            sensorUI.setVisibility(View.GONE);
    }



    // Function to start the measurement
    public void startMeasurement() {
        meas_active = true;
    }

    // Function that gets called when the "Stop Measurement" button is pressed
    public void stopMeasurement(View view) {
        meas_active = false;
        simpleChronometer.stop(); // stop a chronometer
        Log.e("Test", String.valueOf(GVStore.sensorOne_MeasData.acc_z));
        unsubscribe();

        Intent resultDisplayIntent = new Intent(ManualMeasurementSessionActivity.this, ResultDisplayActivity.class);
        startActivity(resultDisplayIntent); // Switch Activity
    }
}
