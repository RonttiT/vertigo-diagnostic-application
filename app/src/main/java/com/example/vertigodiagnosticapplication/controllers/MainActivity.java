package com.example.vertigodiagnosticapplication.controllers;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import com.example.vertigodiagnosticapplication.R;
import com.example.vertigodiagnosticapplication.global_variables.GVStore;
import com.example.vertigodiagnosticapplication.models.MyScanResult;
import com.movesense.mds.Mds;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    // Initialise the variables referencing the layout
    private Button mButtonAddSensors;
    private Button mButtonMeasurementSessionSetup;
    // Not yet used
    private Button mButtonFAQ;
    private Button mButtonSaveData;

    //  Variables for the list of connected Sensors
    private ListView mConnectedSensorListView;
    private ArrayList<MyScanResult> mConnSensArrayList = new ArrayList<>();
    ArrayAdapter<MyScanResult> mConnSensArrayAdapter;



    // Main function that gets launched on the creation of this activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Make sure we have all the permissions this app needs
        requestNeededPermissions();
        // Initializing the Movesense Library
        initMdsObj();

        // Create the list of connected sensors and populate it
        mConnectedSensorListView = findViewById(R.id.main_List_ConnSens);
        mConnSensArrayAdapter = new ArrayAdapter<>( this, R.layout.misc_simple_item, mConnSensArrayList);
        mConnectedSensorListView.setAdapter(mConnSensArrayAdapter); // The array adapter creates the dynamic list layout based on an item template
        populateConnectedSensorList(); // Populate the list

        // Set the layout referencing variables
        mButtonAddSensors = findViewById(R.id.main_button_ConnectNewSensor);
        mButtonMeasurementSessionSetup = findViewById(R.id.main_button_StartMeasuringSession);


        // Listeners and their functions
        // Button listeners
        mButtonAddSensors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sensorConnectingIntent = new Intent(MainActivity.this, SensorConnectionActivity.class);
                startActivity(sensorConnectingIntent); //Switching activity
            }
        });
        mButtonMeasurementSessionSetup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent measurementSessionSetupIntent = new Intent(MainActivity.this, MeasurementSessionSetupActivity.class);
                startActivity(measurementSessionSetupIntent); //Switching activity
            }
        });
    }



    // Function used to save the Movesense library to the GlobalVariable Store
    public void initMdsObj() {
        GVStore.mds_obj = Mds.builder().build(this);
    }


    // Function To populate the connected Sensor List - gets called by the onCreate() function
    void populateConnectedSensorList(){
        mConnSensArrayList.clear();
        mConnSensArrayAdapter.notifyDataSetChanged();
        if(GVStore.ScanResArrayList.size() > 0) {
            Log.d("TAG", GVStore.ScanResArrayList.get(0).macAddress);
            System.out.println(GVStore.ScanResArrayList.get(0));
            Log.d("TAG", String.valueOf(GVStore.ScanResArrayList.get(0).isConnected()));
            int index = 0;
            for (MyScanResult sr : GVStore.ScanResArrayList) {

                if (GVStore.ScanResArrayList.get(index).isConnected()) {
                    mConnSensArrayList.add(0, sr);
                }
                index ++;
            }
            System.out.println(mConnSensArrayList.get(0));
        }
        else{
            Log.d("TAG", "Empty Array");
        }
    }


    // Function asking for permissions - Still needs to be improved so that it asks for it with a popup. Doesn't work at the moment
    void requestNeededPermissions(){
        ActivityResultLauncher<String[]> locationPermissionRequest =
                registerForActivityResult(new ActivityResultContracts
                                .RequestMultiplePermissions(), result -> {
                            Boolean fineLocationGranted = result.getOrDefault(
                                    Manifest.permission.ACCESS_FINE_LOCATION, false);
                            Boolean coarseLocationGranted = result.getOrDefault(
                                    Manifest.permission.ACCESS_COARSE_LOCATION,false);
                            if (fineLocationGranted != null && fineLocationGranted) {
                                // Precise location access granted.
                            } else if (coarseLocationGranted != null && coarseLocationGranted) {
                                // Only approximate location access granted.
                            } else {
                                // No location access granted.
                            }
                        }
                );
        locationPermissionRequest.launch(new String[] {
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        });
    }

}