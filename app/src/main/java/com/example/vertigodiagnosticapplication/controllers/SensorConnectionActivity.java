package com.example.vertigodiagnosticapplication.controllers;


import android.content.Intent;
import com.example.vertigodiagnosticapplication.R;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Button;
import com.example.vertigodiagnosticapplication.global_variables.GVStore;
import com.example.vertigodiagnosticapplication.models.MyScanResult;
import com.movesense.mds.MdsConnectionListener;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsSubscription;
import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.RxBleDevice;
import com.polidea.rxandroidble2.scan.ScanSettings;
import java.util.ArrayList;
import io.reactivex.disposables.Disposable;

public class SensorConnectionActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;

    // Initialise the variables referencing the layout
    private Button mButtonConnect;

    // MDS
    public static final String URI_CONNECTEDDEVICES = "suunto://MDS/ConnectedDevices";
    public static final String URI_EVENTLISTENER = "suunto://MDS/EventListener";
    public static final String SCHEME_PREFIX = "suunto://";

    // BleClient singleton
    static private RxBleClient mBleClient;

    // UI
    private ListView mScanResultListView;
    private ArrayList<MyScanResult> mScanResArrayList = new ArrayList<>();
    ArrayAdapter<MyScanResult> mScanResArrayAdapter;

    // Sensor subscription
    static private String URI_MEAS_ACC_13 = "/Meas/Acc/13";
    private MdsSubscription mdsSubscription;
    private String subscribedDeviceSerial;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_connection);

        // Init Scan UI
        mScanResultListView = findViewById(R.id.sensor_connection_List_ScanResult);
        mScanResArrayAdapter = new ArrayAdapter<>( this, R.layout.misc_simple_item, mScanResArrayList);
        mScanResultListView.setAdapter(mScanResArrayAdapter);
        mScanResultListView.setOnItemLongClickListener(this);
        mScanResultListView.setOnItemClickListener(this);

        // Set the layout referencing variables
        mButtonConnect = findViewById(R.id.sensor_connection_button_Connect);
        mButtonConnect.setEnabled(true);

        // Listeners and their functions
        mButtonConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connectSensors();
            }
        });
    }


    private RxBleClient getBleClient() {
        // Init RxAndroidBle (Ble helper library) if not yet initialized
        if (mBleClient == null)
        {
            mBleClient = RxBleClient.create(this);
        }
        return mBleClient;
    }




    Disposable mScanSubscription;
    public void onScanClicked(View view) {
        findViewById(R.id.sensor_connection_button_Scan).setVisibility(View.GONE);
        findViewById(R.id.sensor_connection_button_ScanStop).setVisibility(View.VISIBLE);

        // Start with empty list
        mScanResArrayList.clear();
        GVStore.ScanResArrayList.clear();
        mScanResArrayAdapter.notifyDataSetChanged();

        mScanSubscription = getBleClient().scanBleDevices(
                new ScanSettings.Builder()
                        // .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY) // change if needed
                        // .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES) // change if needed
                        .build()
                // add filters if needed
        )
                .subscribe(
                        scanResult -> {
                            //Log.d(LOG_TAG,"scanResult: " + scanResult);

                            // Process scan result here. filter movesense devices.
                            if (scanResult.getBleDevice()!=null &&
                                    scanResult.getBleDevice().getName() != null &&
                                    scanResult.getBleDevice().getName().startsWith("Movesense")) {

                                // replace if exists already, add otherwise
                                MyScanResult msr = new MyScanResult(scanResult);
                                if (mScanResArrayList.contains(msr)) {
                                    mScanResArrayList.set(mScanResArrayList.indexOf(msr), msr);
                                    GVStore.ScanResArrayList.set(mScanResArrayList.indexOf(msr), msr);
                                }
                                else {
                                    mScanResArrayList.add(0, msr);
                                    GVStore.ScanResArrayList.add(0, msr);
                                }
                                mScanResArrayAdapter.notifyDataSetChanged();
                            }
                        },
                        throwable -> {
                            Log.e(LOG_TAG,"scan error: " + throwable);
                            // Handle an error here.

                            // Re-enable scan buttons, just like with ScanStop
                            onScanStopClicked(null);
                        }
                );
    }

    // Should make sure that the items can't be selected before this is called.
    public void onScanStopClicked(View view) {
        if (mScanSubscription != null)
        {
            mScanSubscription.dispose();
            mScanSubscription = null;
        }
        Log.d(LOG_TAG,"Test 1: " + mScanResArrayList);

        GVStore.itemSelectionArray = new boolean[mScanResArrayList.size()];

        GVStore.ScanResArrayList_test = mScanResArrayList;
        findViewById(R.id.sensor_connection_button_Scan).setVisibility(View.VISIBLE);
        findViewById(R.id.sensor_connection_button_ScanStop).setVisibility(View.GONE);
    }


    // Function to connecto to sensor
    public void connectSensors(){
        Log.d("Connection", "Looping through Sensors to Connect");

        int index = 0;
        for (boolean selectionStatus : GVStore.itemSelectionArray) {
            if (selectionStatus){
                MyScanResult device = mScanResArrayList.get(index);

                if (!device.isConnected()) {
                // Stop scanning
                onScanStopClicked(null);

                // And connect to the device
                connectBLEDevice(device);
                }
                else {
                // Device is connected - should we do something?
                }
            }
            index++;
        }
    }


    // Function that selects the desired sensors to be connected - Needs to have a visual feedback when a sensor is selected
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position < 0 || position >= mScanResArrayList.size()) {
            return;
        }
        if (GVStore.itemSelectionArray[position]){
            GVStore.itemSelectionArray[position] = false;
        }
        else{
            GVStore.itemSelectionArray[position] = true;
        }
    }

    // Needs to be split and put to the appropriated Acivities
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (position < 0 || position >= mScanResArrayList.size())
            return false;

        MyScanResult device = mScanResArrayList.get(position);

        // unsubscribe if there
        Log.d(LOG_TAG, "onItemLongClick, " + device.connectedSerial + " vs " + subscribedDeviceSerial);
        if (device.connectedSerial.equals(subscribedDeviceSerial))
            unsubscribe();

        Log.i(LOG_TAG, "Disconnecting from BLE device: " + device.macAddress);
        GVStore.mds_obj.disconnect(device.macAddress);

        return true;
    }




    // How to handle the disconnect? Where to put the function, might need to be moved to MainActivity
    private void connectBLEDevice(MyScanResult device) {
        RxBleDevice bleDevice = getBleClient().getBleDevice(device.macAddress);

        Log.i(LOG_TAG, "Connecting to BLE device: " + bleDevice.getMacAddress());
        GVStore.mds_obj.connect(bleDevice.getMacAddress(), new MdsConnectionListener() {

            @Override
            public void onConnect(String s) {
                Log.d(LOG_TAG, "onConnect:" + s);
            }

            @Override
            public void onConnectionComplete(String macAddress, String serial) {
                for (MyScanResult sr : mScanResArrayList) {
                    if (sr.macAddress.equalsIgnoreCase(macAddress)) {
                        sr.markConnected(serial);
                        break;
                    }
                }

                mScanResArrayAdapter.notifyDataSetChanged();
                // Will require to call again connectSensor() until all sensors are connected. then only change activity
                Intent mainIntent = new Intent(SensorConnectionActivity.this, MainActivity.class);
                startActivity(mainIntent);
            }

            @Override
            public void onError(MdsException e) {
                Log.e(LOG_TAG, "onError:" + e);
                showConnectionError(e);
            }

            @Override
            public void onDisconnect(String bleAddress) {

                Log.d(LOG_TAG, "onDisconnect: " + bleAddress);
                for (MyScanResult sr : mScanResArrayList) {
                    if (bleAddress.equals(sr.macAddress))
                    {
                        // unsubscribe if was subscribed
                        if (sr.connectedSerial != null && sr.connectedSerial.equals(subscribedDeviceSerial))
                            unsubscribe();

                        sr.markDisconnected();
                    }
                }
                mScanResArrayAdapter.notifyDataSetChanged();
            }
        });
    }

    private void showConnectionError(MdsException e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Connection Error:")
                .setMessage(e.getMessage());
        builder.create().show();
    }

    private void unsubscribe() {
        if (mdsSubscription != null) {
            mdsSubscription.unsubscribe();
            mdsSubscription = null;
        }
        subscribedDeviceSerial = null;
        // If UI not invisible, do it now
        final View sensorUI = findViewById(R.id.sensor_connection_SensorUI);
        if (sensorUI.getVisibility() != View.GONE)
            sensorUI.setVisibility(View.GONE);
    }

    public void onUnsubscribeClicked(View view) {
        unsubscribe();
    }


}