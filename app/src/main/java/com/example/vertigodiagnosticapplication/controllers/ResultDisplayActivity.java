package com.example.vertigodiagnosticapplication.controllers;

import androidx.appcompat.app.AppCompatActivity;
import com.example.vertigodiagnosticapplication.R;

import android.os.Bundle;

public class ResultDisplayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_display);

        // Step Count function

        // Angle measurement function

        // save button that uses a save model to save the data to a csv (google doc, etc)
        // On succesful save goes back to the main activity

        // retake measurement button -> goes back to the measurement setup activity
    }
}