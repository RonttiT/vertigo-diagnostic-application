package com.example.vertigodiagnosticapplication.controllers;

import android.os.Bundle;
import com.example.vertigodiagnosticapplication.R;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;


public class MeasurementSessionSetupActivity extends AppCompatActivity {

    private Button mButtonMeasurementSession;
    // Main function that gets launched on the creation of this activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement_session_setup);

        // Set the layout referencing variables
        mButtonMeasurementSession = findViewById(R.id.measurement_session_setup_button_StartMeasurement);

        // Listeners and their functions
        // Button listener
        mButtonMeasurementSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent measurementSessionIntent = new Intent(MeasurementSessionSetupActivity.this, ManualMeasurementSessionActivity.class);
                startActivity(measurementSessionIntent); // Switch Activity
            }
        });
    }
}

// This activity shall contain all the test setup options.