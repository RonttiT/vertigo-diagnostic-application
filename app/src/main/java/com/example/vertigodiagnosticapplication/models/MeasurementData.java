package com.example.vertigodiagnosticapplication.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MeasurementData
{
    public ArrayList<String> timestamp = new ArrayList<>();

    public static ArrayList<Double> acc_x = new ArrayList<Double>();
    public ArrayList<Double> acc_y = new ArrayList<Double>();
    public ArrayList<Double> acc_z = new ArrayList<Double>();

    public ArrayList<Double> gyr_x = new ArrayList<>();
    public ArrayList<Double> gyr_y = new ArrayList<>();
    public ArrayList<Double> gyr_z = new ArrayList<>();

    public ArrayList<Double> mag_x = new ArrayList<>();
    public ArrayList<Double> mag_y = new ArrayList<>();
    public ArrayList<Double> mag_z = new ArrayList<>();

}
