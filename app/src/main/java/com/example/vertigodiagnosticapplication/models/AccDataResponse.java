package com.example.vertigodiagnosticapplication.models;

/**
 * Created by lipponep on 22.11.2017.
 */

import com.google.gson.annotations.SerializedName;

public class AccDataResponse {

    @SerializedName("Body")
    public final Body body;

    public AccDataResponse(Body body) {
        this.body = body;
    }

    public static class Body {
        @SerializedName("Timestamp")
        public final long timestamp;

        @SerializedName("ArrayAcc")
        public final AccArray acc_array[];

        @SerializedName("ArrayGyro")
        public final GyroArray[] gyr_array;

        @SerializedName("ArrayMagn")
        public final MagArray[] mag_array;

        @SerializedName("Headers")
        public final Headers header;

        public Body(long timestamp, AccArray[] acc_array, GyroArray[] gyr_array, MagArray[] mag_array, Headers header) {
            this.timestamp = timestamp;
            this.acc_array = acc_array;
            this.gyr_array = gyr_array;
            this.mag_array = mag_array;
            this.header = header;
        }
    }

    public static class AccArray {
        @SerializedName("x")
        public final double x;

        @SerializedName("y")
        public final double y;

        @SerializedName("z")
        public final double z;

        public AccArray(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
    public static class GyroArray {
        @SerializedName("x")
        public final double x;

        @SerializedName("y")
        public final double y;

        @SerializedName("z")
        public final double z;

        public GyroArray(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

    }
    public static class MagArray {
        @SerializedName("x")
        public final double x;

        @SerializedName("y")
        public final double y;

        @SerializedName("z")
        public final double z;

        public MagArray(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public static class Headers {
        @SerializedName("Param0")
        public final int param0;

        public Headers(int param0) {
            this.param0 = param0;
        }
    }
}
