package com.example.vertigodiagnosticapplication.global_variables;

import android.app.Application;

import com.example.vertigodiagnosticapplication.models.MeasurementData;
import com.example.vertigodiagnosticapplication.models.MyScanResult;
import com.movesense.mds.Mds;
import com.example.vertigodiagnosticapplication.models.MyScanResult;
import java.util.ArrayList;
import java.util.Arrays;


public class GVStore extends Application{
    // Test value
    public static String somevalue = "Hello from application singleton!";

    public static Mds mds_obj;

    public static ArrayList<MyScanResult> ScanResArrayList = new ArrayList<>();

    public static ArrayList<MyScanResult> ScanResArrayList_test = new ArrayList<>();

    public static boolean[] itemSelectionArray;


    // Data Saving Variables
    public static MeasurementData sensorOne_MeasData = new MeasurementData();
    public static MeasurementData sensorTwo_MeasData = new MeasurementData();
    public static MeasurementData sensorThree_MeasData = new MeasurementData();

}
